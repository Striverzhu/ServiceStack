﻿using ServiceStack;
using Inn.Ticket;
using Inn.Implementation.Ticket;

namespace Inn.Implementation
{
    public class InnService : Service, IInnService
    {
        private readonly ITicketService ticketService;

        public InnService(ITicketService ticketService)
        {
            this.ticketService = ticketService;
        }

        public TicketResponse Any(TicketRequest request)
        {
            return this.ticketService.GetTicketInfo(request);
        }

        public TicketDelResponse Any(TicketDelRequest request)
        {
            return this.ticketService.DelTicket(request);
        }
    }
}
