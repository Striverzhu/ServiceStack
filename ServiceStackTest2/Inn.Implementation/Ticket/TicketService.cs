﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inn.Ticket;
using Inn.Order;

namespace Inn.Implementation.Ticket
{
    public class TicketService : ITicketService
    {
        public TicketResponse GetTicketInfo(TicketRequest request)
        {
            TicketResponse result = new TicketResponse()
            {
                Orders = new List<OrderResponse>() { new OrderResponse() { OrderId = 1 } },
                ServerId = 1,
                TableNumber = 1,
                TicketId = 1 + request.TicketId,
                Timestamp = DateTime.Now
            };
            return result;
        }

        public TicketDelResponse DelTicket(TicketDelRequest request)
        {
            TicketDelResponse result = new TicketDelResponse();
            if (request.TicketId > 0)
            {
                result.deleteStatus = true;
            }

            return result;
        }
    }
}
