﻿using ServiceStack;
using Inn.Ticket;

namespace Inn
{
    public interface IInnService
    {
        TicketResponse Any(TicketRequest request);
        TicketDelResponse Any(TicketDelRequest deleteid);
    }
}
