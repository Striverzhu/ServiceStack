﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inn.Ticket
{
    public interface ITicketService
    {
        TicketResponse GetTicketInfo(TicketRequest request);
        TicketDelResponse DelTicket(TicketDelRequest request);
    }
}
