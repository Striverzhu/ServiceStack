﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inn.Ticket
{
    public class TicketDelRequest
    {
        public int TicketId { get; set; }
    }
}
