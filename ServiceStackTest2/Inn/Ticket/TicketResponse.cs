﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inn.Order;

namespace Inn.Ticket
{
    public class TicketResponse
    {
        public int TicketId { get; set; }
        public int TableNumber { get; set; }
        public int ServerId { get; set; }
        public List<OrderResponse> Orders { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
