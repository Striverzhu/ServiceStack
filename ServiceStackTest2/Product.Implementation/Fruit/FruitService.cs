﻿using Product.Fruit;

namespace Product.Implementation.Fruit
{
    public class FruitService : IFruitService
    {
        public GetFruitWeightResponse GetFruitWeight(GetFruitWeightRequest request)
        {
            GetFruitWeightResponse response = new GetFruitWeightResponse();
            if (request.fruitNumber < 0)
            {
                return response;
            }
            response.fruitTotalWeight = request.fruitNumber * request.fruitPerWeight;
            response.fruitID = request.fruitID;

            return response;
        }

        public GetFruitPriceResponse GetFruitPrice(GetFruitPriceRequest request)
        {
            GetFruitPriceResponse response = new GetFruitPriceResponse();
            if (request.fruitID < 0)
                return response;
            response.fruitTotalPrice = request.fruitNumber * request.fruitPerPrice;
            response.fruitID = request.fruitID;
            response.fruitPerPrice = request.fruitPerPrice;

            return response;
        }
    }
}
