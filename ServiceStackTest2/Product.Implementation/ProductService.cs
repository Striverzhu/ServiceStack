﻿using Product.Implementation.Fruit;
using Product.Fruit;
using ServiceStack;

namespace Product.Implementation
{
    public class ProductService : Service, IProductService
    {
        private readonly IFruitService fruitService;
        public ProductService(IFruitService fruitService)
        {
            this.fruitService = fruitService;
        }

        [Route("/Product/GetFruitWeight", "POST")]
        public GetFruitWeightResponse Any(GetFruitWeightRequest request)
        {
            return fruitService.GetFruitWeight(request);
        }

        [Route("/Product/GetFruitPrice", "POST")]
        public GetFruitPriceResponse Any(GetFruitPriceRequest request)
        {
            return fruitService.GetFruitPrice(request);
        }
    }
}
