﻿using ServiceStack;

namespace Product.Fruit
{
    public class GetFruitPriceRequest
    {
        public int fruitID { get; set; }
        public int fruitNumber { get; set; }
        public int fruitPerPrice { get; set; }
    }
}
