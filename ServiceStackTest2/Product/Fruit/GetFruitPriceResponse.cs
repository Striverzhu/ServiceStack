﻿namespace Product.Fruit
{
    public class GetFruitPriceResponse
    {
        public int fruitID { get; set; }
        public int fruitPerPrice { get; set; }
        public int fruitTotalPrice { get; set; }
    }
}
