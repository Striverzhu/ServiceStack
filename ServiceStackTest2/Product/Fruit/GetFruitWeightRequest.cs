﻿namespace Product.Fruit
{
    public class GetFruitWeightRequest
    {
        public int fruitID { get; set; }
        public int fruitNumber { get; set; }
        public int fruitPerWeight { get; set; }
    }
}
