﻿namespace Product.Fruit
{
    public class GetFruitWeightResponse
    {
        public int fruitID { get; set; }
        public int fruitTotalWeight { get; set; }
    }
}
