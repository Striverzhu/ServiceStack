﻿namespace Product.Fruit
{
    public interface IFruitService
    {
        GetFruitWeightResponse GetFruitWeight(GetFruitWeightRequest request);
        GetFruitPriceResponse GetFruitPrice(GetFruitPriceRequest request);
    }
}
