﻿using Product.Fruit;

namespace Product
{
    public interface IProductService
    {
        GetFruitWeightResponse Any(GetFruitWeightRequest request);
        GetFruitPriceResponse Any(GetFruitPriceRequest request);
    }
}
