﻿using ServiceStack;
using Inn.Implementation;
using Product.Implementation;
using Inn.Implementation.Ticket;
using Product.Implementation.Fruit;
using Inn.Ticket;
using Product.Fruit;

namespace Server.Host
{
    public class ServiceHost : AppHostBase
    {
        private readonly static System.Reflection.Assembly[] assembly = new System.Reflection.Assembly[]
        {
            typeof(ProductService).Assembly,
            typeof(InnService).Assembly
        };

        //// Register your Web service with ServiceStack.
        public ServiceHost(string WebName) : base(WebName, assembly) { }

        public override void Configure(Funq.Container container)
        {
            // Register any dependencies your services use here.
            Routes
                  .Add<GetFruitPriceRequest>("/getPrice")
                  .Add<GetFruitWeightRequest>("/getWeight")
                  .Add<TicketRequest>("/getTicket")
                  .Add<TicketDelRequest>("/delTicket");

            Plugins.Add(new CorsFeature(allowedMethods: "GET, POST"));

            this.Register<ITicketService>(new TicketService());
            this.Register<IFruitService>(new FruitService());
        }
    }
}
